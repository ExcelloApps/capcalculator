import Vue from 'vue';
import App from './App.vue';
import router from './router';
import Button from '@/components/Button';
import VueI18n from 'vue-i18n';
import ar from './lang/ar';
import en from './lang/en';

Vue.component('btn', Button);

Vue.config.productionTip = false;

// Filters
Vue.filter('round', value => Math.round(value));
Vue.filter('toFixed', value => {
    return isFloat(value) ? value.toFixed(1) : value;
});
Vue.filter('toFixed2', value => {
    return isFloat(value) ? value.toFixed(2) : value;
});
Vue.filter('toFixed4', value => {
    return isFloat(value) ? value.toFixed(4) : value;
});
Vue.filter(
    'formatMoney',
    (amount, decimalCount = 0, decimal = '.', thousands = ',') => {
        // console.log(amount);
        // console.log(isFloat(amount));
        if (!isFloat(amount)) {
            decimalCount = 0;
        } else {
            decimalCount = 1;
        }
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? '-' : '';

            let i = parseInt(
                (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
            ).toString();
            let j = i.length > 3 ? i.length % 3 : 0;

            return (
                negativeSign +
                (j ? i.substr(0, j) + thousands : '') +
                i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
                (decimalCount
                    ? decimal +
                      Math.abs(amount - i)
                          .toFixed(decimalCount)
                          .slice(2)
                    : '')
            );
        } catch (e) {
            console.log(e);
        }
    }
);

// helpers
function isFloat(n) {
    // return n === +n && n !== (n | 0);
    return Number(n) === n && n % 1 !== 0;
}

//i18n-setup.js
Vue.use(VueI18n);

const messages = {
    en,
    ar
};

const i18n = new VueI18n({
    locale: 'ar', // set locale
    // fallbackLocale: 'ar',
    messages // set locale messages
});

new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
