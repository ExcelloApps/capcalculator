import Vue from 'vue';
import Router from 'vue-router';
// import Home from './views/Home.vue';
// import NoConvertibleNotes from './views/NoConvertibleNotes.vue';
import ConvertibleNotes from './views/ConvertibleNotes.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            component: ConvertibleNotes
        }
        // {
        //     path: '/no-convertible-notes',
        //     name: 'no-convertible-notes',
        //     component: NoConvertibleNotes
        // },
        // {
        //     path: '/convertible-notes',
        //     name: 'convertible-notes',
        //     component: Home
        // }
    ]
});
